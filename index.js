const express = require("express");
const path = require("path");
const logger = require("morgan");
const bodyParser = require("body-parser");
// const dotenv = require("dotenv").config();
const cors = require("cors");
const app = express();
const axios = require("axios");

const port = process.env.PORT || 5000;

app.use(
  cors({ origin: ["http://localhost:3000", "https://www.googleapis.com/"] })
);

app.use(express.static(path.join(__dirname, "client/build")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Handle React routing, return all requests to React app
app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "client/build", "index.html"));
});

app.post("/geocode", function(req, res) {
  // console.log("horses of courses", req.body);

  /**
   * @param {{View:object}} data
   */


  axios({
    method: "get",
    url:
      "https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?app_id=ccPPO4FRTv377o8hTDFS&app_code=eOT3Kg5mGrH9C1yPQYHUKg&mode=retrieveAddresses&maxresults=100&gen=9&prox=" +
      req.body[0] +
      "%2C" +
      req.body[1] +
      "%2C" +
      100
  }).then(function(response) {
    res.send(response.data.Response.View[0].Result);
  });
});

app.listen(port, () => console.log(`Listening on port ${port}`));

const mooch = 10;

const hours = (num) => num * 24;
const minutes = (num) => num * 24 * 60;
const seconds = (num) => num * 24 * 60 * 60;

const moochHours = hours(mooch);
const moochMinutes = minutes(mooch);
const moochSeconds = seconds(mooch);

const deca = (num) => {
    a = num * 1e1;
    return Math.round(a)
};
const hecto = (num) => {
    a = num * 1e2;
    return Math.round(a)
};
const kilo = (num) => {
    a = num * 1e3;
    return Math.round(a)
};
const mega = (num) => {
    a = num * 1e6;
    return Math.round(a)
};
const giga = (num) => {
    a = num * 1e9;
    return Math.round(a)
};
const tera = (num) => {
    a = num * 1e12;
    return Math.round(a)
};
const peta = (num) => {
    a = num * 1e15;
    return Math.round(a)
};
const exa = (num) => {
    a = num * 1e18;
    return Math.round(a)
};
const zeta = (num) => {
    a = num * 1e21;
    return Math.round(a)
};
const yotta = (num) => {
    a = num * 1e24;
    return Math.round(a)
};

const deci = (num) => {
    a = num * 1e-1;
    return a.toPrecision(1);
};
const centi = (num) => {
    a = num * 1e-2;
    return a.toPrecision(2);
};
const milli = (num) => {
    a = num * 1e-3;
    return a.toPrecision(3);
};
const micro = (num) => {
    a = num * 1e-6;
    return a.toPrecision(6);
};
const nano = (num) => {
    a = num * 1e-9;
    return a.toPrecision(9);
};
const pico = (num) => {
    a = num * 1e-12;
    return a.toPrecision(12);
};
const femto = (num) => {
    a = num * 1e-15;
    return a.toPrecision(15);
};
const atto = (num) => {
    a = num * 1e-18;
    return a.toPrecision(18);
};
const zepto = (num) => {
    a = num * 1e-21;
    return a.toPrecision(21);
};
const yocto = (num) => {
    a = num * 1e-24;
    return a.toPrecision(24);
};

const convertedObject = (days) => ({
    deca: deca(days),
    hecto: hecto(days),
    kilo: kilo(days),
    mega: mega(days),
    giga: giga(days),
    tera: tera(days),
    peta: peta(days),
    exa: exa(days),
    zeta: zeta(days),
    yotta: yotta(days),
    deci: deci(days),
    centi: centi(days),
    milli: milli(days),
    micro: micro(days),
    nano: nano(days),
    pico: pico(days),
    femto: femto(days),
    atto: atto(days),
    zepto: zepto(days),
    yocto: yocto(days)
});



import React, {Component} from 'react';
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink} from 'reactstrap';
import PropTypes from 'prop-types';
// import {AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler} from '@coreui/react';
// import logo from '../../assets/img/brand/logo.svg'
// import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.politicsToggle = this.politicsToggle.bind(this);
    this.state = {
      dropdownOpen: false,
      politicsDropdownOpen: false,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  politicsToggle() {
    this.setState({
      politicsDropdownOpen: !this.state.politicsDropdownOpen
    });
  }

  render() {

    // eslint-disable-next-line
    const {children, ...attributes} = this.props;

    return (
      <React.Fragment>
        {/*<AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />*/}

        <Nav className="d-md-down-none offset-2" style={{maxWidth: "1140px"}} navbar>
          <NavItem className="px-3">
            <NavLink href="/"><h5 className="m-0 p-0">Mike Duran-Mitchell</h5></NavLink>
          </NavItem>
          <Dropdown nav isOpen={this.state.politicsDropdownOpen} toggle={this.politicsToggle} className="px-3">
            <DropdownToggle nav>
              Politics
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem><NavLink href="/politics">Politics Homepage</NavLink></DropdownItem>
              <DropdownItem><NavLink href="/politics/how-many-mooches">How Many Mooches?</NavLink></DropdownItem>
              <DropdownItem><NavLink href="/politics/ads-dems-should-make">Ads Dems Should Make</NavLink></DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle} className="px-3">
            <DropdownToggle nav>
              Arboriculture
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem><NavLink href="/arboriculture">Arboriculture Homepage</NavLink></DropdownItem>
              <DropdownItem><NavLink href="/arboriculture/tree-removal-calculator">Tree Removal
                Calculator</NavLink></DropdownItem>
            </DropdownMenu>
          </Dropdown>

          <NavItem className="px-3">
            <NavLink href="/programming">Programming</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="/the-biopolitical-forest">The Biopolitical Forest</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="/about-mike-duran-mitchell">About Mike</NavLink>
          </NavItem>
        </Nav>
        {/*        <Nav className="m-auto" navbar>
          <NavItem className="d-md-down-none">
            <NavLink href="/"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="/"><i className="icon-list"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="/"><i className="icon-location-pin"></i></NavLink>
          </NavItem>*/}
        {/* <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              <DropdownItem divider />
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              <DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>*/}
        {/*</Nav>*/}
        {/*   <AppAsideToggler className="d-md-down-none" />*/}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;

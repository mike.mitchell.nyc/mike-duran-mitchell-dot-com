import "react-dates/lib/css/_datepicker.css";
import React, { Component } from "react";
import {
  Card,
  CardBody,
  Container,
  CardSubtitle,

  Table,
  Row,
  CardTitle,
  Button,
  Col,
} from "reactstrap";
import staffers from "./staffers.json";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import moment from "moment";

console.log(staffers);
const mooch = 10;

// const hours = (num) => num * 24;
// const minutes = (num) => num * 24 * 60;
// const seconds = (num) => num * 24 * 60 * 60;
//
// const moochHours = hours(mooch);
// const moochMinutes = minutes(mooch);
// const moochSeconds = seconds(mooch);

const deca = (num) => {
  const a = num * 1e1;
  return Math.round(a);
};
const hecto = (num) => {
  const a = num * 1e2;
  return Math.round(a);
};
const kilo = (num) => {
  const a = num * 1e3;
  return Math.round(a);
};
const mega = (num) => {
  const a = num * 1e6;
  return Math.round(a);
};
const giga = (num) => {
  const a = num * 1e9;
  return Math.round(a);
};
const tera = (num) => {
  const a = num * 1e12;
  return Math.round(a);
};
const peta = (num) => {
  const a = num * 1e15;
  return Math.round(a);
};
const exa = (num) => {
  const a = num * 1e18;
  return Math.round(a);
};
const zeta = (num) => {
  const a = num * 1e21;
  return Math.round(a);
};
const yotta = (num) => {
  const a = num * 1e24;
  return Math.round(a);
};

const deci = (num) => {
  const a = num * 1e-1;
  return a.toPrecision(1);
};
const centi = (num) => {
  const a = num * 1e-2;
  return a.toPrecision(2);
};
const milli = (num) => {
  const a = num * 1e-3;
  return a.toPrecision(3);
};
const micro = (num) => {
  const a = num * 1e-6;
  return a.toPrecision(6);
};
const nano = (num) => {
  const a = num * 1e-9;
  return a.toPrecision(9);
};
const pico = (num) => {
  const a = num * 1e-12;
  return a.toPrecision(12);
};
const femto = (num) => {
  const a = num * 1e-15;
  return a.toPrecision(15);
};
const atto = (num) => {
  const a = num * 1e-18;
  return a.toPrecision(18);
};
const zepto = (num) => {
  const a = num * 1e-21;
  return a.toPrecision(21);
};
const yocto = (num) => {
  const a = num * 1e-24;
  return a.toPrecision(24);
};

const moochMaker = (days) => ({
  deca: deca(days),
  hecto: hecto(days),
  kilo: kilo(days),
  mega: mega(days),
  giga: giga(days),
  tera: tera(days),
  peta: peta(days),
  exa: exa(days),
  zeta: zeta(days),
  yotta: yotta(days),
  deci: deci(days),
  centi: centi(days),
  milli: milli(days),
  micro: micro(days),
  nano: nano(days),
  pico: pico(days),
  femto: femto(days),
  atto: atto(days),
  zepto: zepto(days),
  yocto: yocto(days)
});


class Mooch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment([2017, 6, 21]),
      endDate: moment([2017, 6, 31]),
      mooches: 0,
      days: 0,
      allMooches: ""
    };
    this.handleMoochInput = this.handleMoochInput.bind(this);
  }

  componentDidMount() {
    const duration = moment.duration(this.state.endDate.diff(this.state.startDate));
    const notSanitizedDays = duration.asDays();
    const days = Math.abs(notSanitizedDays);
    const mooches = days / mooch;
    const allMooches = moochMaker(mooches);
    this.setState({ mooches, days, allMooches });
  }

  handleMoochInput() {
    console.log(this.state);

    const duration = moment.duration(this.state.endDate.diff(this.state.startDate));
    const notSanitizedDays = duration.asDays();
    const days = Math.abs(notSanitizedDays);
    const mooches = days / mooch;
    const allMooches = moochMaker(mooches);
    this.setState({ mooches, days, allMooches });


  }

  render() {
    return (

      <Container fluid style={{ maxWidth: "1200px" }}>
        <Row className="pt-4 pb-3">
          <Row className="bg-white shadow-lg mx-auto text-left p-4" style={{ maxWidth: "1000px" }}>
            \ <h1 className="text-center">Mooch Calculator - Days In Office</h1>

            <p className="text-muted text-center">A calculator based on Anthony Scaramucci's Famously Short Trump
              Cabinet Stint.
            </p>
            <br/>
            <p>In 2013, NPR penned a piece about <a
              href="https://www.npr.org/templates/story/story.php?storyId=251330321">
              the rarity of firing a cabinet member</a>, letting people know within the context of the Kathleen
              Sebelius debate.</p>

            <p>In 2017, Anthony Scaramucci became the poster child for how poorly the Trump administration has been
              built and maintained when he was fired after 10 days after he was appointed the White House Director of
              Communications.</p>
            <p>A reddit user came up with the units of a mooch, which further inspired <a
              href="https://www.reddit.com/r/theydidthemooch/">r/theydidthemooch</a>, a subreddit dedicated to
              tracking how many mooches Trump's cabinet members survive. or about the length of Scaramucci's time in
              office.
            </p>
            <p>Enter your start and end dates to see how many mooches your
              timeline is!</p>

            <Card className="m-1 p-3 w-100">
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" lg="3">
                    <Card className="text-white bg-info">
                      <CardBody className="pb-0">
                        <h3>{this.state.days}</h3>
                        <h1>Days</h1>
                      </CardBody>
                      <div className="chart-wrapper mx-3" style={{ height: "70px" }}>
                      </div>
                    </Card>
                  </Col>

                  <Col xs="12" sm="6" lg="3">
                    <Card className="text-white bg-primary">
                      <CardBody className="pb-0">
                        <h3>{this.state.mooches}</h3>
                        <h1>{this.state.mooches !== 1 ? "Mooches" : "Mooch"}</h1>
                      </CardBody>
                      <div className="chart-wrapper mx-3" style={{ height: "70px" }}>
                      </div>
                    </Card>
                  </Col>
                  <Col xs="12" sm="6" lg="6">
                    <CardTitle>The Whole Mooch and Nothing but the Mooch</CardTitle>
                    <CardSubtitle>
                      <Button size="small" color='link'>
                        I want all units of mooch!
                      </Button>
                    </CardSubtitle>
                    <DateRangePicker
                      isOutsideRange={() => false}
                      startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                      startDateId="startDateId" // PropTypes.string.isRequired,
                      endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                      endDateId="endDateId" // PropTypes.string.isRequired,
                      onDatesChange={({ startDate, endDate }) => this.setState({
                          startDate,
                          endDate
                        },
                        () => this.state.endDate && this.state.startDate ? this.handleMoochInput() : "")} // PropTypes.func.isRequired,
                      focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                      onFocusChange={
                        focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                    />
                  </Col>
                </Row>
              </CardBody>
            </Card>

            {
              /*
               }*/
              this.state.mooches !== "" ?
                (<Table>
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Days</th>
                    <th>Weeks</th>
                    <th>Years</th>
                  </tr>
                  </thead>
                  <tbody>
                  <TableProducer mooches={this.state.mooches}/>
                  </tbody>
                </Table>) : ""
            }
          </Row>
        </Row>
      </Container>
    );
  }
}

const TableProducer = (props) => {
  let moochData = [];
  for (const key of Object.keys(props.mooches)) {
    moochData.push([key, props.mooches[key]]);
  }
  return (moochData.map((data,
                         i) => (<tr key={i + "moochCount"}>
    <td>{data[0]}</td>
    <td>{data[1]}</td>
  </tr>)));
};


export default Mooch;

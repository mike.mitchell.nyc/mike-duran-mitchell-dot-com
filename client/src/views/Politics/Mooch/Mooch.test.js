import React from 'react';
import ReactDOM from 'react-dom';
import Mooch from './Mooch';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Mooch />, div);
  ReactDOM.unmountComponentAtNode(div);
});

import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import { Col, Container, Row, Card } from "reactstrap";

const AnyReactComponent = ({ text }) => <div>{text}</div>;
// add collapsible sidebar
// key AIzaSyBqYAKa8S5vZSNLvOl5EvrmgPJJxDxe3Vs
class Homepage extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 14
  };

  state = { userLocation: { lat: 32, lng: 32 }, zoom: 14, loading: true };

  componentDidMount(props) {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;

        this.setState({
          userLocation: { lat: latitude, lng: longitude },
          loading: false
        });
      },
      () => {
        this.setState({ loading: false });
      }
    );
  }

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     map: null
  //   };
  //   // this.dropPin = this.dropPin.bind(this);
  // }

  render() {
    const { loading, userLocation } = this.state;

    if (loading) {
      return null;
    }
    return (

      <Container fluid className="d-md-flex justify-content-center bg-blue" style={{ maxWidth: "1600px" }}>
        <Col className="col-sm-12 col-md-3"><Card className="m-1" style={{ height: "80vh" }}></Card>
        </Col>
        <Col className="col-sm-12 col-md-9 m-1" style={{ height: "80vh" }}>

          <GoogleMapReact
            bootstrapURLKeys={{ key: "AIzaSyBqYAKa8S5vZSNLvOl5EvrmgPJJxDxe3Vs" }}
            defaultCenter={userLocation}
            defaultZoom={this.state.zoom}
          >
            <AnyReactComponent
              lat={59.955413}
              lng={30.337844}
              text={"Kreyser Avrora"}
            />
          </GoogleMapReact>

        </Col>
        <
        /Container>
        );
        }
        }

        export default Homepage;

import React from "react";
import { MapControl, withLeaflet } from "react-leaflet";

class LocateControlImpl extends MapControl {
  createLeafletElement(props) {
    const { options, startDirectly } = props;
    const { map } = props.leaflet;
    const lc = L.control.locate(options).addTo(map);

    if (startDirectly)
      setTimeout(() => {
        lc.start();
      }, 0);
    return lc;
  }
}

const LocateControl = withLeaflet(LocateControlImpl);

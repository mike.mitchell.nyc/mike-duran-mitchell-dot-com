import React from 'react';
import ReactDOM from 'react-dom';
import AdsDemsShouldMake from './AdsDemsShouldMake';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AdsDemsShouldMake />, div);
  ReactDOM.unmountComponentAtNode(div);
});

import React from 'react';
import ReactDOM from 'react-dom';
import TreeRemovalCalculator from './TreeRemovalCalculator';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TreeRemovalCalculator />, div);
  ReactDOM.unmountComponentAtNode(div);
});

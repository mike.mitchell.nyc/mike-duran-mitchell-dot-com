import React, { Component } from "react";
import {
  Container
} from "reactstrap";
import ProfileImage from "../../assets/img/mike-duran-mitchell-profile.JPG";

class Home extends Component {
  render() {
    return (

      <Container className="mx-auto">

        <img className="img-fluid picture mx-auto d-block"
             src={ProfileImage}
             alt="Picture of Mike Duran-Mitchell"/>

      </Container>
    )
      ;
  }
}

export default Home;

import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Container,
} from 'reactstrap';

class TheBiopoliticalForest extends Component {
  render() {
    return (
      <Container>
        <Card className="p-4 w-75 border-0">
          <CardBody>
            <CardTitle className="mb-3">The Biopolitical Forest - A Blog About Politics, Trees, and Everything
              Else</CardTitle>
            <CardSubtitle className="mx-2 mb-3">Subtitle goes here I think</CardSubtitle>
            <Card className="p-4 w-75 border-0">
              <CardBody>
                <CardTitle className="mb-3">My Title Goes Here and they may be pretty long so we have to test
                  this</CardTitle>
                <CardSubtitle className="mx-2 mb-3">Subtitle goes here I think</CardSubtitle>
                <CardText>Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.Some quick example text to build on the card title and make up the bulk of the card's
                  content.</CardText>
                <Button>Button</Button>
              </CardBody>
            </Card>
          </CardBody>
        </Card>


      </Container>

    );
  }
}

export default TheBiopoliticalForest;

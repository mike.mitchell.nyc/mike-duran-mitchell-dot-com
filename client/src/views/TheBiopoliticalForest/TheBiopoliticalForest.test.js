import React from 'react';
import ReactDOM from 'react-dom';
import TheBiopoliticalForest from './TheBiopoliticalForest';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TheBiopoliticalForest />, div);
  ReactDOM.unmountComponentAtNode(div);
});
